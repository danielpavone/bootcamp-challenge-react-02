## Desafio 2

- Nesse desafio você irá melhorar a aplicação que desenvolvemos durante o segundo módulo com as seguintes funcionalidades:

- Armazene a lista de repositórios adicionados ao localStorage (https://www.w3schools.c om/html/html5_webstorage.asp), ou seja, se a página for atualizada ou fechada/aberta a lista de repositórios deve ser a mesma;

- Crie um botão em cada repositório para deletar o mesmo da listagem e remova-o do localStorage também;

- Crie um botão em cada repositório para atualizar os dados dele buscando as novas informações de stars, forks, last commit, etc;
